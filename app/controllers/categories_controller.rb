class CategoriesController < ApplicationController

  before_action :require_admin, except: %i[index show]
  before_action :get_category, only: %i[show edit update destroy]


  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:notice] = 'category was created successfully'
      redirect_to @category
    else
      render 'new'
    end
  end

  def index
    @category = Category.all
  end

  def show
    @articles = @category.articles
  end

  def edit

  end

  def destroy
    @category.destroy
    redirect_to categories_path
  end

  private

  def get_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name)
  end

  def require_admin
    if !(logged_in? && current_user.admin?)
      flash[:alert] = 'Only admin scan perform this function'
      redirect_to categories_path
    end
  end

end