Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'pages#home'
  get 'about', to: 'pages#about'
  resources :articles, only: %i[show index new create edit update delete destroy]

  get 'signup', to: 'users#new'
  delete 'delete', to: 'users#destroy'
  get 'delete', to: 'users#destroy'
  resources :users, except: %i[new destroy]

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  get 'logout', to: 'sessions#destroy'

  resources :categories
end
